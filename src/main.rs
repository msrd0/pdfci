extern crate yaml_rust;

mod config;
use config::Config;

fn main()
{
    let conf : Config = Config::parse(String::from("/home/msrd0/git/pdfci/config.yml")).unwrap();
    println!("Using port {}", conf.server.port)
}
