
use yaml_rust;
use yaml_rust::YamlLoader;

use std::io;
use std::io::prelude::*;
use std::fs::File;
use std::error::Error;
use std::vec::Vec;

pub struct Config
{
	pub server : ConfigServer,
	pub pdfci : ConfigPdfci,
	pub git : Vec<ConfigGit>
}

pub struct ConfigServer
{
	pub port : u16,
	pub listen4 : String,
	pub listen6 : String
}

pub struct ConfigPdfci
{
	pub shared_dir : String
}

pub struct ConfigGit
{
	pub name : String,
	pub host : String,
	pub clone : String
}

#[derive(Debug)]
pub enum ConfigParseError
{
	IoError(io::Error),
	YamlError(yaml_rust::ScanError)
}

impl From<io::Error> for ConfigParseError
{
	#[inline]
	fn from(error : io::Error) -> ConfigParseError
	{
		return ConfigParseError::IoError(error);
	}
}

impl From<yaml_rust::ScanError> for ConfigParseError
{
	#[inline]
	fn from(error : yaml_rust::ScanError) -> ConfigParseError
	{
		return ConfigParseError::YamlError(error);
	}
}

impl Config
{
	pub fn parse(filename : String) -> Result<Config, ConfigParseError>
	{
		let mut f = File::open(filename)?;
		let mut buf = String::new();
		f.read_to_string(&mut buf)?;
		let yaml = YamlLoader::load_from_str(&buf[..])?;

		let mut c = Config {
			server: ConfigServer {
				port: 8099,
				listen4: String::from("0.0.0.0"),
				listen6: String::from("::")
			},
			pdfci: ConfigPdfci {
				shared_dir: String::from("/var/lib/pdfci")
			},
			git: Vec::new()
		};

		return Ok(c);
	}
}
